# Amazon Translate Connector
Provides translation between one source language and another of the same set of languages.

Documentation: https://docs.aws.amazon.com/translate

Specification: https://github.com/APIs-guru/openapi-directory/blob/main/APIs/amazonaws.com/translate/2017-07-01/openapi.yaml

## Prerequisites

+ Generate an AWS Acess Key ID and AWS Secret Key from the AWS Admin Console

## Supported Operations
All endpoints are operational.
## Connector Feedback

Feedback can be provided directly to Product Management in our [Product Feedback Forum](https://community.boomi.com/s/ideas) in the boomiverse.  When submitting an idea, please provide the full connector name in the title and a detailed description.

